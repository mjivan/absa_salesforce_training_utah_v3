/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.ABSA_Salesforce_POC_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Testing.PageObjects.SalesForcePageObjects;

/**
 *
 * @author MJivan
 */

@KeywordAnnotation
(
    Keyword = "NavigateAndSignIntoSalesforce",
    createNewBrowserInstance = false
)
public class SalesforceSignInTest  extends BaseClass
{
 public TestEntity testData;
    String error = "";
    Narrator narrator; 
    
    public SalesforceSignInTest(TestEntity testData)
    {
        this.testData=testData;
        narrator = new Narrator(testData);
    }
    
    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the salesforce URL
        if (!NavigateToSalesforce())
        {
            return narrator.testFailed("Failed to navigate to salesforce sign in - " + error);
        }

        // This step will sign into the specified salesforce account with the provided credentials
        if (!SignIntoSalesforce())
        {
            return narrator.testFailed("Failed to sign into salesforce - " + error);
        }

        return narrator.finalizeTest("Successfully Navigated through salesforce login page");
    }

    public boolean NavigateToSalesforce()
    {

        if (!SeleniumDriverInstance.navigateTo(GmailPageObject.GmailURL()))
        {
            error = "Failed to navigate to salesforce.";
            return false;
        }

          if(!SeleniumDriverInstance.enterTextByXpath(SalesForcePageObjects.LoginUsername(),testData.getData("Email Address")))
        {
            error = "Failed to enter username into the username field";
            return false;
        }
        return true;
    }

    public boolean SignIntoSalesforce()
    {
     
        if(!SeleniumDriverInstance.enterTextByXpath(SalesForcePageObjects.LoginUsername(),testData.getData("Email Address")))
        {
            error = "Failed to enter username into the username field";
            return false;
        }

        return true;

    }

}
