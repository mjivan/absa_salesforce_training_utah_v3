/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.GmailPageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author fnell
 */
@KeywordAnnotation(
        Keyword = "CreateNewGmailAccount",
        createNewBrowserInstance = true
)
public class GmailCreateAccount extends BaseClass {

    public TestEntity testData;
    String error = "";
    Narrator narrator;

    public GmailCreateAccount(TestEntity testData) {
        this.testData = testData;
        narrator = new Narrator(testData);

    }

    public TestResult executeTest() {
        // This step will Launch the browser and navigate to the GMail URL
        if (!NavigateToGmailSignUp()) {
            return narrator.testFailed("Failed to navigate to the next page - " + error);
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignUpToGmailAccount()) {
            return narrator.testFailed("Failed to navigate to the next page - " + error);
        }

        return narrator.finalizeTest("Successfully Navigated to the next page");
    }

    public boolean NavigateToGmailSignUp() {

        if (!SeleniumDriverInstance.navigateTo(GmailPageObject.GmailURL())) {
            error = "Failed to navigate to gmail.";
            return false;
        }

        return true;
    }

    public boolean SignUpToGmailAccount() {
        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.firstNameTextBox(), testData.getData("Firstname"))) {
            error = "Failed to enter '" + testData.getData("Firstname") + "' into firstname field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered firstname");
        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.createAccountNext())) {
            error = "Failed to click Next button";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.lastNameTextBox(), testData.getData("Lastname"))) {
            error = "Failed to enter '" + testData.getData("Lastnames") + "' into lastname field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered lastname");
        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.createAccountNext())) {
            error = "Failed to click Next button (1)";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.userNameTextBox(), testData.getData("Username"))) {
            error = "Failed to enter '" + testData.getData("Username") + "' into username field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered username");
        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.createAccountNext())) {
            error = "Failed to click Next button (2)";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.passWordTextbox(), testData.getData("Password"))) {
            error = "Failed to enter '" + testData.getData("Password") + "' into password field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered password");

        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.createAccountNext())) {
            error = "Failed to click Next button (3)";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.confirmPassword(), testData.getData("ConfirmPassword"))) {
            error = "Failed to match '" + testData.getData("ConfirmPassword") + "' to password field.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully entered Confirmation password");
        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.createAccountNext())) {
            error = "Failed to click Next button (4)";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered all details for creating gmail account");

        if (!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.phoneNumberTextBox(), testData.getData("Phone"))) {
            error = "Failed to enter '" + testData.getData("Phone") + "' into the phone number field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered phone number");
        if (!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.phoneNumberNext())) {
            error = "Failed to click Next button (1)";
            return false;
        }
        return true;
    }

}
