/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;
import KeywordDrivenTestFramework.Core.BaseClass;
/**
 *
 * @author MJivan
 */
public class SalesForcePageObjects extends BaseClass
{
     public static String SalesForceURL()
    {
        // Use ENUM
        return currentEnvironment.PageUrl;
    }
     
     public static String LoginUsername()
     {
         return "//input[@id='username']";
     }
     
     public static String LoginPassword()
     {
         return "//input[@id='password']";
     }
     
     public static String LoginButton()
     {
         return "//input[@id='Login']";
     }

}
