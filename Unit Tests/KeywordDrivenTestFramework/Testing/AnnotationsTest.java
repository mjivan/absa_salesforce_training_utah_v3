/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Entities.DataColumn;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import java.io.IOException;
import static java.lang.System.err;
import static java.lang.System.out;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.junit.Test;

/**
 *
 * @author fnell
 */
public class AnnotationsTest 
{
    //This is a prototype test to determine the value of reducing the Switch-based keyword/testclass matching to a system of class annotations 
    //Which define testsclasses' keyword parameters during runtime...
    
    
    
    
    
    @Test
    public void runKeywordDrivenTests() throws IOException
    {
       
       
       ClassLoader lc = ClassLoader.getSystemClassLoader();
       
       
       ClassPath cp = ClassPath.from(ClassLoader.getSystemClassLoader());
       
       ImmutableSet<ClassPath.ClassInfo> allClasses = cp.getTopLevelClassesRecursive("KeywordDrivenTestFramework.Testing");
       
       for(ClassPath.ClassInfo info : allClasses)
        {
            out.println("[CLASSES LISTED] - " + info.getName());
            out.println("[Annotations for Class] - " + info.load().getAnnotations().length);
            

        }
      
       Stream<ClassPath.ClassInfo> annotatedClasses;
       Predicate<ClassPath.ClassInfo> hasAnnotationPredicate = c-> c.load().isAnnotationPresent(KeywordAnnotation.class);
       annotatedClasses =  allClasses.stream().filter(hasAnnotationPredicate);
     
        
        Predicate<ClassPath.ClassInfo> checkKeywordPredicate = c-> c.load().getAnnotation(KeywordAnnotation.class).Keyword().contains("Gmail");
        ClassPath.ClassInfo  obj = annotatedClasses.filter(checkKeywordPredicate).findFirst().get();
        
        
        
        out.println("[CLASS FOUND] - " + obj.getName());

            

        
    }
    
    public ClassInfo getKeywordTestClass(String keywordName)
    {
       try
       {
        
            //Get list of all loaded classes for the package - defined at runtime - we need to be able to isolate just the TestClasses
            // in order to extract the one matching the keyword to be executed
            ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());                  
            
            //Next we set up Predicates (a type of query in java) to isolate the list of classes to only those pertaining to the framework
            // i.e. no dependencies included 
            ImmutableSet<ClassPath.ClassInfo> allClasses = classPath.getTopLevelClassesRecursive("KeywordDrivenTestFramework.Testing");
            
            //We then filter the classes to only those who have the required annotations - annotations used to add meta
            //data to the TestClasses sothat we can scan them to read their Keywords - this uses Lambda notation only available in Java 8 and above.
            Predicate<ClassPath.ClassInfo> hasAnnotationPredicate = c-> c.load().isAnnotationPresent(KeywordAnnotation.class);
            Stream<ClassPath.ClassInfo> annotatedClasses =  allClasses.stream().filter(hasAnnotationPredicate);
            
            //The filtered list is then queried a second time in order to retrieve the valid TestClass based on the keywordName
            Predicate<ClassPath.ClassInfo> checkKeywordPredicate = c-> c.load().getAnnotation(KeywordAnnotation.class).Keyword().equals(keywordName);
            ClassPath.ClassInfo  testClass = annotatedClasses.filter(checkKeywordPredicate).findFirst().get();
            
            if(testClass == null)
            {
                err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: Keyword not found");
            }
            
            return testClass;
            
            
       }
       catch(Exception ex)
       {
           err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: " + ex.getMessage());
           
           return null;
       }
    }
}
