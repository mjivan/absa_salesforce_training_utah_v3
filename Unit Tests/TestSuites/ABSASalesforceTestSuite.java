/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author MJivan
 */
public class ABSASalesforceTestSuite 
{
        static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public ABSASalesforceTestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.SALESFORCE;
    }

    @Test
    public void SalesForcePOCTest() throws FileNotFoundException
    {
        Narrator.logDebug("Salesforce - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\ABSA TEST PACK.xlsx", Enums.BrowserType.Chrome,true);
        instance.runKeywordDrivenTests();
    }
}
